# git-box
git-box clean and smudge filter enabling binary off-repo external storage.

## Status
[![Build Status](https://travis-ci.org/rustyhorde/git-box.svg?branch=master)](https://travis-ci.org/rustyhorde/git-box)
[![Latest Version](https://img.shields.io/crates/v/git-box.svg)](https://crates.io/crates/git-box)
[![Coverage Status](https://coveralls.io/repos/github/rustyhorde/git-box/badge.svg?branch=master)](https://coveralls.io/github/rustyhorde/git-box?branch=master)

## Installation
* Latest Release - ```cargo install git-box```
* Bleeding Edge - ```cargo install git-box --git https://github.com/rustyhorde/git-box.git```
* Cloned Copy - ```cargo install git-box --path /path/to/git-box```

Running the install will place the ```git-box```, ```git-box-clean```, and
```git-box-smudge``` binaries in your .cargo directory.  Ensure that you add
```~/.cargo/bin``` path to your PATH environment variable. Run
```which git-box``` to ensure the executable can be found.
