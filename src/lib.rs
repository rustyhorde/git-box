//! Shared API for git-box, git-box-clean, and git-box-smudge.
#![cfg_attr(feature = "clippy", feature(plugin))]
#![cfg_attr(feature = "clippy", plugin(clippy))]
#![cfg_attr(feature = "clippy", deny(clippy, clippy_pedantic))]
#![cfg_attr(any(feature = "google", feature="dropbox"), feature(custom_derive, plugin))]
#![cfg_attr(any(feature = "google", feature="dropbox"), plugin(serde_macros))]
#![deny(missing_docs)]
#[cfg(any(feature = "google", feature = "s3"))]
extern crate chrono;
#[cfg(any(feature = "artifactory", feature = "dropbox", feature = "google", feature = "s3"))]
extern crate curl;
extern crate docopt;
extern crate git2;
#[cfg(any(feature = "dropbox", feature = "google"))]
extern crate hyper;
#[macro_use]
extern crate log;
#[cfg(feature = "mysql")]
extern crate mysql;
#[cfg(feature = "postgres")]
extern crate openssl;
#[cfg(feature = "postgres")]
extern crate postgres;
extern crate regex;
#[cfg(feature = "rusqlite")]
extern crate rusqlite;
extern crate rustc_serialize;
#[cfg(any(feature = "dropbox", feature = "google"))]
extern crate serde;
#[cfg(any(feature = "dropbox", feature = "google"))]
extern crate serde_json;
#[cfg(feature = "artifactory")]
extern crate sha1;
extern crate sodium_sys;
#[cfg(feature = "ssh2")]
extern crate ssh2;
#[cfg(any(feature = "dropbox", feature = "google"))]
extern crate url;
#[cfg(any(feature = "dropbox", feature = "google"))]
extern crate url_open;
#[cfg(feature = "s3")]
extern crate urlparse;
#[cfg(feature = "s3")]
extern crate warheadhateus;

pub use config::{Configure, GitBoxConfigResult, box_storage, open_config};
pub use error::GitBoxError;
pub use filter::{Clean, Filter, GitBoxFilterResult, Smudge};
#[cfg(feature = "archiva")]
pub use filter::repo::archiva::ArchivaBox;
#[cfg(feature = "artifactory")]
pub use filter::repo::art::ArtBox;
#[cfg(feature = "dropbox")]
pub use filter::cloud::dropbox::DropboxBox;
#[cfg(feature = "file")]
pub use filter::file::FileBox;
#[cfg(feature = "google")]
pub use filter::cloud::google::GoogleDriveBox;
#[cfg(feature = "mysql")]
pub use filter::db::mysql::MysqlBox;
#[cfg(feature = "nexus")]
pub use filter::repo::nexus::NexusBox;
#[cfg(feature = "postgres")]
pub use filter::db::postgres::PostgresBox;
#[cfg(feature = "s3")]
pub use filter::cloud::s3::S3Box;
#[cfg(feature = "rusqlite")]
pub use filter::db::sqlite::SqliteBox;
#[cfg(feature = "ssh2")]
pub use filter::ssh2::Ssh2Box;
use log::LogLevel::Debug;

pub mod config;
mod error;
mod filter;

#[derive(Debug,RustcDecodable)]
/// command line arguments
pub struct Args {
    /// Artifactory API Key
    pub arg_apikey: String,
    /// Database Name
    pub arg_db: Option<String>,
    /// Hostname
    pub arg_host: String,
    /// Password
    pub arg_password: String,
    /// Path
    pub arg_path: String,
    /// Pattern
    pub arg_pattern: String,
    /// AWS Region
    pub arg_region: String,
    /// Artifactory Repository
    pub arg_repository: String,
    /// Artifactory Subfolder
    pub arg_subfolder: String,
    /// Table Name
    pub arg_table: String,
    /// Artifactory URL
    pub arg_url: String,
    /// User
    pub arg_user: String,
    /// Configuring Archiva?
    pub cmd_archiva: bool,
    /// Configuring Artifactory?
    pub cmd_art: bool,
    /// Configuring?
    pub cmd_config: bool,
    /// Configuring Dropbox?
    pub cmd_dropbox: bool,
    /// Configuring File?
    pub cmd_file: bool,
    /// Configuring Google Drive?
    pub cmd_google: bool,
    /// Configuring Nexus?
    pub cmd_nexus: bool,
    /// Configuring Mysql?
    pub cmd_mysql: bool,
    /// Configuring Postgres?
    pub cmd_postgres: bool,
    /// Configuring Sqlite?
    pub cmd_sqlite: bool,
    /// Configuring S3?
    pub cmd_s3: bool,
    /// Configuring SSH?
    pub cmd_ssh: bool,
    /// Track?
    pub cmd_track: bool,
    /// Untrack?
    pub cmd_untrack: bool,
    /// Configure at the global level?
    pub flag_global: bool,
    /// Optional database name.
    pub flag_d: Option<String>,
    /// Optional Hostname.
    pub flag_h: Option<String>,
    /// Is help requested?
    pub flag_help: bool,
    /// Optional Password.
    pub flag_p: Option<String>,
    /// Optional Port.
    pub flag_port: Option<u16>,
    /// Prefer unix sockets?
    pub flag_prefer: bool,
    /// Remove configuration requested?
    pub flag_remove: bool,
    /// Optional Unix socket address.
    pub flag_socket: Option<String>,
    /// Is SSL enabled?
    pub flag_ssl: bool,
    /// Optional table name.
    pub flag_t: Option<String>,
    /// Optional Username.
    pub flag_u: Option<String>,
    /// Supplying a token?
    pub flag_token: Option<String>,
    /// Is verbose output requested?
    pub flag_verbose: bool,
    /// Is SSL peer verification requested?
    pub flag_verify: bool,
    /// Is version information requested?
    pub flag_version: bool,
}

fn enabled() -> bool {
    log_enabled!(Debug)
}

/// Convert a byte vector to a hex string.
pub fn to_hex(input: &[u8]) -> String {
    let mut s = String::new();
    for b in input.iter() {
        s.push_str(&format!("{:02x}", *b));
    }
    s
}
