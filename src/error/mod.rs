#[cfg(any(feature = "artifactory", feature = "s3"))]
use curl::ErrCode;
use docopt;
use git2;
#[cfg(feature = "mysql")]
use mysql;
#[cfg(feature = "postgres")]
use openssl;
#[cfg(feature = "postgres")]
use postgres;
#[cfg(feature = "rusqlite")]
use rusqlite;
#[cfg(feature = "google")]
use rustc_serialize;
#[cfg(feature = "google")]
use serde_json;
use sodium_sys::SSError;
#[cfg(feature = "ssh2")]
use ssh2::Error as Ssh2Error;
use std::error::Error;
use std::fmt;
use std::io;
use std::num::ParseIntError;
use std::string::FromUtf8Error;
#[cfg(feature = "s3")]
use warheadhateus;

#[derive(Debug)]
pub enum ParseErrorType {
    Int(ParseIntError),
    #[cfg(feature = "s3")]
    Region(warheadhateus::ParseRegionError),
    #[cfg(feature = "google")]
    RSJson(rustc_serialize::json::ParserError),
    #[cfg(feature = "google")]
    SerdeJson(serde_json::Error),
    Str(FromUtf8Error),
}

impl fmt::Display for ParseErrorType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ParseErrorType::Int(ref e) => writeln!(f, "{}", e),
            #[cfg(feature = "s3")]
            ParseErrorType::Region(ref e) => writeln!(f, "{}", e),
            #[cfg(feature = "google")]
            ParseErrorType::RSJson(ref e) => writeln!(f, "{}", e),
            #[cfg(feature = "google")]
            ParseErrorType::SerdeJson(ref e) => writeln!(f, "{}", e),
            ParseErrorType::Str(ref e) => writeln!(f, "{}", e),
        }
    }
}

#[derive(Debug)]
/// An error thrown by the git-box process.
pub enum GitBoxError {
    #[cfg(any(feature = "artifactory", feature = "s3"))]
    /// Error thrown during Curl operations.
    Curl(ErrCode),
    /// Error thrown parsing arguments.
    Docopt(docopt::Error),
    /// Error thrown during git operations.
    Git(git2::Error),
    /// Error thrown during I/O operations.
    IO(io::Error),
    #[cfg(feature = "mysql")]
    /// Error thrown during Mysql operations.
    Mysql(mysql::error::MyError),
    #[cfg(feature = "postgres")]
    /// Error thrown during Posgtres operations.
    Postgres(postgres::error::Error),
    #[cfg(feature = "postgres")]
    /// Error thrown during Posgtres connect operations.
    PostgresConnect(postgres::error::ConnectError),
    /// Error thrown during sodium_sys operations.
    Sodium(SSError),
    /// Error thrown during parsing operations.
    Parse(ParseErrorType),
    #[cfg(feature = "s3")]
    /// Error thrown during S3 operations.
    S3(warheadhateus::AWSAuthError),
    #[cfg(feature = "rusqlite")]
    /// Error thrown during Sqlite operations.
    Sqlite(rusqlite::Error),
    #[cfg(feature = "ssh2")]
    /// Error thrown during ssh operations.
    SSH(Ssh2Error),
    #[cfg(feature = "postgres")]
    /// Error thrown during ssl operations over postgres connections.
    SSL(openssl::ssl::error::SslError),
    /// Generic Error.
    Other(&'static str),
}

#[cfg_attr(feature = "clippy", allow(use_debug))]
impl fmt::Display for GitBoxError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            #[cfg(any(feature = "artifactory", feature = "s3"))]
            GitBoxError::Curl(ref e) => writeln!(f, "{}", e),
            GitBoxError::Docopt(ref e) => writeln!(f, "{}", e),
            GitBoxError::Git(ref e) => writeln!(f, "{}", e),
            GitBoxError::IO(ref e) => writeln!(f, "{}", e),
            #[cfg(feature = "mysql")]
            GitBoxError::Mysql(ref e) => writeln!(f, "{}", e),
            GitBoxError::Sodium(ref e) => writeln!(f, "{:?}", e),
            GitBoxError::Parse(ref e) => writeln!(f, "{}", e),
            #[cfg(feature = "postgres")]
            GitBoxError::PostgresConnect(ref e) => writeln!(f, "{}", e),
            #[cfg(feature = "postgres")]
            GitBoxError::Postgres(ref e) => writeln!(f, "{}", e),
            #[cfg(feature = "s3")]
            GitBoxError::S3(ref e) => writeln!(f, "{}", e),
            #[cfg(feature = "rusqlite")]
            GitBoxError::Sqlite(ref e) => writeln!(f, "{}", e),
            #[cfg(feature = "ssh2")]
            GitBoxError::SSH(ref e) => writeln!(f, "{}", e),
            #[cfg(feature = "postgres")]
            GitBoxError::SSL(ref e) => writeln!(f, "{}", e),
            GitBoxError::Other(ref e) => writeln!(f, "{}", e),
        }
    }
}

impl Error for GitBoxError {
    fn description(&self) -> &str {
        match *self {
            #[cfg(feature = "artifactory")]
            GitBoxError::Curl(ref e) => e.description(),
            GitBoxError::Docopt(ref e) => e.description(),
            GitBoxError::Git(ref e) => e.description(),
            GitBoxError::IO(ref e) => e.description(),
            #[cfg(feature = "mysql")]
            GitBoxError::Mysql(ref e) => e.description(),
            GitBoxError::Sodium(_) => "sodium_sys error",
            GitBoxError::Parse(ref e) => {
                match *e {
                    ParseErrorType::Int(ref pe) => pe.description(),
                    #[cfg(feature = "s3")]
                    ParseErrorType::Region(ref pe) => pe.description(),
                    #[cfg(feature = "google")]
                    ParseErrorType::RSJson(ref pe) => pe.description(),
                    #[cfg(feature = "google")]
                    ParseErrorType::SerdeJson(ref pe) => pe.description(),
                    ParseErrorType::Str(ref pe) => pe.description(),
                }
            }
            #[cfg(feature = "postgres")]
            GitBoxError::PostgresConnect(ref e) => e.description(),
            #[cfg(feature = "postgres")]
            GitBoxError::Postgres(ref e) => e.description(),
            #[cfg(feature = "s3")]
            GitBoxError::S3(ref e) => e.description(),
            #[cfg(feature = "rusqlite")]
            GitBoxError::Sqlite(ref e) => e.description(),
            #[cfg(feature = "postgres")]
            GitBoxError::SSL(ref e) => e.description(),
            #[cfg(feature = "ssh2")]
            GitBoxError::SSH(ref e) => e.description(),
            GitBoxError::Other(ref e) => e,
        }
    }

    fn cause(&self) -> Option<&Error> {
        match *self {
            #[cfg(feature = "artifactory")]
            GitBoxError::Curl(ref e) => Some(e),
            GitBoxError::Docopt(ref e) => Some(e),
            GitBoxError::Git(ref e) => Some(e),
            GitBoxError::IO(ref e) => Some(e),
            #[cfg(feature = "mysql")]
            GitBoxError::Mysql(ref e) => Some(e),
            GitBoxError::Sodium(_) |
            GitBoxError::Other(_) => None,
            GitBoxError::Parse(ref e) => {
                match *e {
                    ParseErrorType::Int(ref pe) => Some(pe),
                    #[cfg(feature = "s3")]
                    ParseErrorType::Region(ref pe) => Some(pe),
                    #[cfg(feature = "google")]
                    ParseErrorType::RSJson(ref pe) => Some(pe),
                    #[cfg(feature = "google")]
                    ParseErrorType::SerdeJson(ref pe) => Some(pe),
                    ParseErrorType::Str(ref pe) => Some(pe),
                }
            }
            #[cfg(feature = "postgres")]
            GitBoxError::PostgresConnect(ref e) => Some(e),
            #[cfg(feature = "postgres")]
            GitBoxError::Postgres(ref e) => Some(e),
            #[cfg(feature = "s3")]
            GitBoxError::S3(ref e) => Some(e),
            #[cfg(feature = "rusqlite")]
            GitBoxError::Sqlite(ref e) => Some(e),
            #[cfg(feature = "ssh2")]
            GitBoxError::SSH(ref e) => Some(e),
            #[cfg(feature = "postgres")]
            GitBoxError::SSL(ref e) => Some(e),
        }
    }
}

#[cfg(any(feature = "artifactory", feature = "s3"))]
impl From<ErrCode> for GitBoxError {
    fn from(err: ErrCode) -> GitBoxError {
        GitBoxError::Curl(err)
    }
}

#[cfg(feature = "mysql")]
impl From<mysql::error::MyError> for GitBoxError {
    fn from(err: mysql::error::MyError) -> GitBoxError {
        GitBoxError::Mysql(err)
    }
}

#[cfg(feature = "postgres")]
impl From<postgres::error::ConnectError> for GitBoxError {
    fn from(err: postgres::error::ConnectError) -> GitBoxError {
        GitBoxError::PostgresConnect(err)
    }
}

#[cfg(feature = "postgres")]
impl From<postgres::error::Error> for GitBoxError {
    fn from(err: postgres::error::Error) -> GitBoxError {
        GitBoxError::Postgres(err)
    }
}

#[cfg(feature = "postgres")]
impl From<openssl::ssl::error::SslError> for GitBoxError {
    fn from(err: openssl::ssl::error::SslError) -> GitBoxError {
        GitBoxError::SSL(err)
    }
}

#[cfg(feature = "rusqlite")]
impl From<rusqlite::Error> for GitBoxError {
    fn from(err: rusqlite::Error) -> GitBoxError {
        GitBoxError::Sqlite(err)
    }
}

#[cfg(feature = "s3")]
impl From<warheadhateus::AWSAuthError> for GitBoxError {
    fn from(err: warheadhateus::AWSAuthError) -> GitBoxError {
        GitBoxError::S3(err)
    }
}

#[cfg(feature = "s3")]
impl From<warheadhateus::ParseRegionError> for GitBoxError {
    fn from(err: warheadhateus::ParseRegionError) -> GitBoxError {
        GitBoxError::Parse(ParseErrorType::Region(err))
    }
}

#[cfg(feature = "google")]
impl From<rustc_serialize::json::ParserError> for GitBoxError {
    fn from(err: rustc_serialize::json::ParserError) -> GitBoxError {
        GitBoxError::Parse(ParseErrorType::RSJson(err))
    }
}

#[cfg(feature = "google")]
impl From<serde_json::Error> for GitBoxError {
    fn from(err: serde_json::Error) -> GitBoxError {
        GitBoxError::Parse(ParseErrorType::SerdeJson(err))
    }
}

impl From<docopt::Error> for GitBoxError {
    fn from(err: docopt::Error) -> GitBoxError {
        GitBoxError::Docopt(err)
    }
}

impl From<git2::Error> for GitBoxError {
    fn from(err: git2::Error) -> GitBoxError {
        GitBoxError::Git(err)
    }
}

impl From<io::Error> for GitBoxError {
    fn from(err: io::Error) -> GitBoxError {
        GitBoxError::IO(err)
    }
}

impl From<SSError> for GitBoxError {
    fn from(err: SSError) -> GitBoxError {
        GitBoxError::Sodium(err)
    }
}

#[cfg(feature = "ssh2")]
impl From<Ssh2Error> for GitBoxError {
    fn from(err: Ssh2Error) -> GitBoxError {
        GitBoxError::SSH(err)
    }
}

impl From<ParseIntError> for GitBoxError {
    fn from(err: ParseIntError) -> GitBoxError {
        GitBoxError::Parse(ParseErrorType::Int(err))
    }
}

impl From<FromUtf8Error> for GitBoxError {
    fn from(err: FromUtf8Error) -> GitBoxError {
        GitBoxError::Parse(ParseErrorType::Str(err))
    }
}
