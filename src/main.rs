//! Binary Off-repo eXternal Storage
//!
//! git-box allows you to store non-diffable files in a storage location away from the repository.
//! The file that gets stored in the repository is a reference to the actual binary.  When you
//! perform a checkout your working copy file is retrieved from the storage location.
//!
//! # Supported Storage Types
//! * file - Store the binary at a file path.
//! * ssh - Store the binary over ssh/sftp at a file path.
//! * artifactory - Store the binary in an Artifactory repository.
//! * mysql - Store the binary in a Mysql database.
//! * postgres - Store the binary in a Postgres database.
//! * sqlite - Store the binary in a Sqlite database.
//!
//! # General Usage (New Repository)
//! 1. ```mkdir sample-repo && cd sample-repo```
//! 2. ```git init```
//! 3. ```git box config```
//! 4. ```git box config file /data/dir/for/binaries```
//! 5. ```git box track "**/*.png"``` - Note that quotes are necessary to avoid shell expansion.
//! 6. ```cp some_cool.png .```
//! 7. ```git add .``` - Note there should be 2 files added, .gitattributes is added by step 5.
//! 8. ```git commit -m "initial commit"```
//!
//! Now check your ```/data/dir/for/binaries``` directory.  You should see a file (named as a
//! hash). This is your binary.  The file that was stored in the git repository is only a reference
//! to this file.
#![cfg_attr(feature = "clippy", feature(plugin))]
#![cfg_attr(feature = "clippy", plugin(clippy))]
#![cfg_attr(feature = "clippy", deny(clippy, clippy_pedantic))]
#![cfg_attr(feature = "clippy", allow(needless_borrow))]
#![deny(missing_docs)]
extern crate docopt;
extern crate libgitbox;
#[macro_use]
extern crate log;
extern crate rustc_serialize;
extern crate sodium_sys;

use docopt::Docopt;
use libgitbox::{Args, Configure, GitBoxConfigResult, GitBoxError, config, open_config};
#[cfg(feature = "archiva")]
use libgitbox::ArchivaBox;
#[cfg(feature = "artifactory")]
use libgitbox::ArtBox;
#[cfg(feature = "dropbox")]
use libgitbox::DropboxBox;
#[cfg(feature = "file")]
use libgitbox::FileBox;
#[cfg(feature = "google")]
use libgitbox::GoogleDriveBox;
#[cfg(feature = "mysql")]
use libgitbox::MysqlBox;
#[cfg(feature = "nexus")]
use libgitbox::NexusBox;
#[cfg(feature = "postgres")]
use libgitbox::PostgresBox;
#[cfg(feature = "s3")]
use libgitbox::S3Box;
#[cfg(feature = "rusqlite")]
use libgitbox::SqliteBox;
#[cfg(feature = "ssh2")]
use libgitbox::Ssh2Box;
use log::{Log, LogLevelFilter, LogMetadata, LogRecord, SetLoggerError};
use sodium_sys::crypto::utils::init;
use std::io::{self, Write};
use std::process;
use std::sync::{ONCE_INIT, Once};

mod version;

static START: Once = ONCE_INIT;

fn init_sodium() {
    START.call_once(|| {
        init::init();
    });
}

#[cfg_attr(rustfmt, rustfmt_skip)]
static USAGE: &'static str = "Usage:
    git-box config [-gvr]
    git-box config archiva [-gv] (<user> <password> <url> <repository> <subfolder> | -r)
    git-box config art [-gv] (<user> <apikey> <url> <repository> <subfolder> | -r)
    git-box config dropbox [-gvr]
    git-box config file [-gv] (<path> | -r)
    git-box config google [-gv] ([--token <token>] | -r)
    git-box config mysql [-gv] ([-sV --prefer -u <user> -p <password> -h <host> --port <port> \
--socket <sa> -d <db> -t <table>] | -r)
    git-box config nexus [-gv] (<user> <password> <url> <repository> <subfolder> | -r)
    git-box config postgres [-gv] (<user> [-s -p <password> -h <host> --port <port> -d <db> \
-t <table>] | -r)
    git-box config s3 [-gv] ([<url> <region>] | -r)
    git-box config sqlite [-gv] (<path> <table> | -r)
    git-box config ssh [-gv] (<user> <host> <path> | -r)
    git-box (track | untrack) [-v] <pattern>
    git-box --help
    git-box [-v] --version

Options:
    -d <db>          Configure the given database name.
    -g --global      Configure globally.
    -h <host>        Configure the given hostname.
    --help           Show this message.
    -p <password>    Configure the given password.
    --port <port>    Configure the given port.
    --prefer         Prefer a socket connection when possible.
    -r --remove      Remove configuration.
    -s --ssl         Enable ssl.
    --socket <sa>    Configure the given unix socket.
    -t <table>       Configure the given table name.
    --token <token>  Supply a refresh token.
    -u <user>        Configure the given username
    -V --verify      Verify peer.
    -v --verbose     Turn on verbose output (stderr).
    --version        Show version information.";

fn parse_cmd_args<T: Configure>(remove: bool, global: bool, config: T) -> GitBoxConfigResult {
    if remove {
        try!(config.remove_config(&mut try!(open_config(global))));
    } else {
        try!(config.set_config(&mut try!(open_config(global))));
    }
    Ok(0)
}

#[cfg(feature = "archiva")]
fn parse_archiva_args(remove: bool, global: bool, args: Args) -> GitBoxConfigResult {
    parse_cmd_args(remove, global, ArchivaBox::from(args))
}

#[cfg(not(feature = "archiva"))]
fn parse_archiva_args(_: bool, _: bool, _: Args) -> GitBoxConfigResult {
    Err(GitBoxError::Other("archiva support not built into this version of git-box!"))
}

#[cfg(feature = "artifactory")]
fn parse_art_args(remove: bool, global: bool, args: Args) -> GitBoxConfigResult {
    parse_cmd_args(remove, global, ArtBox::from(args))
}

#[cfg(not(feature = "artifactory"))]
fn parse_art_args(_: bool, _: bool, _: Args) -> GitBoxConfigResult {
    Err(GitBoxError::Other("artifactory support not built into this version of git-box!"))
}

#[cfg(feature = "file")]
fn parse_file_args(remove: bool, global: bool, args: Args) -> GitBoxConfigResult {
    parse_cmd_args(remove, global, FileBox::from(args))
}

#[cfg(not(feature = "file"))]
fn parse_file_args(_: bool, _: bool, _: Args) -> GitBoxConfigResult {
    Err(GitBoxError::Other("file support not built into this version of git-box!"))
}

#[cfg(feature = "mysql")]
fn parse_mysql_args(remove: bool, global: bool, args: Args) -> GitBoxConfigResult {
    parse_cmd_args(remove, global, MysqlBox::from(args))
}

#[cfg(not(feature = "mysql"))]
fn parse_mysql_args(_: bool, _: bool, _: Args) -> GitBoxConfigResult {
    Err(GitBoxError::Other("mysql support not built into this version of git-box!"))
}

#[cfg(feature = "nexus")]
fn parse_nexus_args(remove: bool, global: bool, args: Args) -> GitBoxConfigResult {
    parse_cmd_args(remove, global, NexusBox::from(args))
}

#[cfg(not(feature = "nexus"))]
fn parse_nexus_args(_: bool, _: bool, _: Args) -> GitBoxConfigResult {
    Err(GitBoxError::Other("nexus support not built into this version of git-box!"))
}

#[cfg(feature = "postgres")]
fn parse_postgres_args(remove: bool, global: bool, args: Args) -> GitBoxConfigResult {
    parse_cmd_args(remove, global, PostgresBox::from(args))
}

#[cfg(not(feature = "postgres"))]
fn parse_postgres_args(_: bool, _: bool, _: Args) -> GitBoxConfigResult {
    Err(GitBoxError::Other("postgres support not built into this version of git-box!"))
}

#[cfg(feature = "s3")]
fn parse_s3_args(remove: bool, global: bool, args: Args) -> GitBoxConfigResult {
    parse_cmd_args(remove, global, S3Box::from(args))
}

#[cfg(not(feature = "s3"))]
fn parse_s3_args(_: bool, _: bool, _: Args) -> GitBoxConfigResult {
    Err(GitBoxError::Other("ssh2 support not built into this version of git-box!"))
}

#[cfg(feature = "ssh2")]
fn parse_ssh2_args(remove: bool, global: bool, args: Args) -> GitBoxConfigResult {
    parse_cmd_args(remove, global, Ssh2Box::from(args))
}

#[cfg(not(feature = "ssh2"))]
fn parse_ssh2_args(_: bool, _: bool, _: Args) -> GitBoxConfigResult {
    Err(GitBoxError::Other("ssh2 support not built into this version of git-box!"))
}

#[cfg(feature = "rusqlite")]
fn parse_sqlite_args(remove: bool, global: bool, args: Args) -> GitBoxConfigResult {
    parse_cmd_args(remove, global, SqliteBox::from(args))
}

#[cfg(not(feature = "rusqlite"))]
fn parse_sqlite_args(_: bool, _: bool, _: Args) -> GitBoxConfigResult {
    Err(GitBoxError::Other("sqlite support not built into this version of git-box!"))
}

#[cfg(feature = "dropbox")]
fn parse_dropbox_args(remove: bool, global: bool, args: Args) -> GitBoxConfigResult {
    // Special case here.  Dropbox configuration is heavy (opens browser, etc.).  So if we are
    // removing, just create an empty default config and move on.
    let config = if remove {
        Default::default()
    } else {
        DropboxBox::from(args)
    };
    parse_cmd_args(remove, global, config)
}

#[cfg(not(feature = "dropbox"))]
fn parse_dropbox_args(_: bool, _: bool, _: Args) -> GitBoxConfigResult {
    Err(GitBoxError::Other("dropbox support not built into this version of git-box!"))
}

#[cfg(feature = "google")]
fn parse_google_args(remove: bool, global: bool, args: Args) -> GitBoxConfigResult {
    // Special case here.  Google Drive configuration is heavy (opens browser, etc.).  So if we are
    // removing, just create an empty default config and move on.
    let config = if remove {
        Default::default()
    } else {
        GoogleDriveBox::from(args)
    };
    parse_cmd_args(remove, global, config)
}

#[cfg(not(feature = "google"))]
fn parse_google_args(_: bool, _: bool, _: Args) -> GitBoxConfigResult {
    Err(GitBoxError::Other("dropbox support not built into this version of git-box!"))
}

fn parse_config_args(args: Args) -> GitBoxConfigResult {
    let global = args.flag_global;
    let remove = args.flag_remove;

    if args.cmd_archiva {
        parse_archiva_args(remove, global, args)
    } else if args.cmd_art {
        parse_art_args(remove, global, args)
    } else if args.cmd_dropbox {
        parse_dropbox_args(remove, global, args)
    } else if args.cmd_file {
        parse_file_args(remove, global, args)
    } else if args.cmd_google {
        parse_google_args(remove, global, args)
    } else if args.cmd_mysql {
        parse_mysql_args(remove, global, args)
    } else if args.cmd_nexus {
        parse_nexus_args(remove, global, args)
    } else if args.cmd_postgres {
        parse_postgres_args(remove, global, args)
    } else if args.cmd_s3 {
        parse_s3_args(remove, global, args)
    } else if args.cmd_ssh {
        parse_ssh2_args(remove, global, args)
    } else if args.cmd_sqlite {
        parse_sqlite_args(remove, global, args)
    } else if !args.flag_remove {
        config::set_filter(&mut try!(config::open_config(args.flag_global)))
    } else if args.flag_remove {
        config::unset_filter(&mut try!(config::open_config(args.flag_global)))
    } else {
        Err(GitBoxError::Other("Unknown Arguments!"))
    }
}

fn parse_args(args: Args) -> GitBoxConfigResult {
    if args.flag_version {
        writeln!(io::stdout(), "{}", version::version(args.flag_verbose))
            .expect("Error writing version to stdout!");
        Ok(0)
    } else if args.cmd_track {
        config::track(&args.arg_pattern[..])
    } else if args.cmd_untrack {
        config::untrack(&args.arg_pattern[..])
    } else if args.cmd_config {
        parse_config_args(args)
    } else {
        Err(GitBoxError::Other("Unknown Arguments!"))
    }
}

struct SimpleLogger;

impl Log for SimpleLogger {
    fn enabled(&self, _metadata: &LogMetadata) -> bool {
        true
    }

    fn log(&self, record: &LogRecord) {
        writeln!(io::stdout(), "{}", record.args()).expect("Error writing to stdout!");
    }
}

fn init(verbose: bool) -> Result<(), SetLoggerError> {
    let lvl = if verbose {
        LogLevelFilter::Debug
    } else {
        LogLevelFilter::Info
    };

    log::set_logger(|max_log_level| {
        max_log_level.set(lvl);
        Box::new(SimpleLogger)
    })
}

/// git-box entry point
#[cfg_attr(feature = "clippy", allow(use_debug))]
pub fn main() {
    let args: Args = Docopt::new(USAGE)
        .and_then(|d| d.decode())
        .unwrap_or_else(|e| e.exit());
    init_sodium();
    init(args.flag_verbose).expect("Unable to initialize logging");

    match parse_args(args) {
        Ok(exit_code) => process::exit(exit_code),
        Err(e) => {
            writeln!(io::stderr(), "{}", e).expect("Error writing to stderr!");
            process::exit(1)
        }
    }
}
