use error::GitBoxError;

#[cfg(any(feature = "mysql", feature = "postgres", feature = "rusqlite"))]
pub mod db;
#[cfg(feature = "file")]
pub mod file;
#[cfg(any(feature = "archiva", feature = "artifactory", feature = "nexus", feature = "s3"))]
pub mod repo;
#[cfg(feature = "ssh2")]
pub mod ssh2;
#[cfg(any(feature = "dropbox", feature = "s3"))]
pub mod cloud;

/// General result type for git-box operations.
pub type GitBoxFilterResult = Result<usize, GitBoxError>;

/// Clean trait used by the clean filter.
pub trait Clean {
    /// Run the clean operation.
    fn clean(&self, buf: &mut Vec<u8>, sha1: &str, size: usize) -> GitBoxFilterResult;
}

/// Smudge trait used by the smudge filter.
pub trait Smudge {
    /// Run the smudge operation.
    fn smudge(&self, buf: &mut Vec<u8>, sha1: &str, size: usize) -> GitBoxFilterResult;
}

/// A git-box filter must implement both Clean & Smudge.
pub trait Filter: Clean + Smudge {}
