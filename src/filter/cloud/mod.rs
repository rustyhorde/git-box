#[cfg(feature = "dropbox")]
pub mod dropbox;
#[cfg(feature = "google")]
pub mod google;
#[cfg(feature = "s3")]
pub mod s3;

#[cfg_attr(rustfmt, rustfmt_skip)]
#[cfg(any(feature = "dropbox", feature="google"))]
const SUCC_RESP_HTML: &'static str = "<!DOCTYPE html>
<html lang=\"en\">
  <head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <title>git-box</title>
    <link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\" \
          rel=\"stylesheet\" \
          integrity=\"sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7\" \
          crossorigin=\"anonymous\">
  </head>
  <body>
    <div class=\"container\" role=\"main\">
      <h1>SUCCESS!</h1>
      <h2>You may safely close this page.</h2>
    </div>
    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"
            integrity=\"sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS\" \
            crossorigin=\"anonymous\"></script>
  </body>
</html>";
#[cfg_attr(rustfmt, rustfmt_skip)]
#[cfg(any(feature = "dropbox", feature="google"))]
const FAIL_RESP_HTML: &'static str = "<!DOCTYPE html>
<html lang=\"en\">
  <head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <title>git-box</title>
    <link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\" \
          rel=\"stylesheet\" \
          integrity=\"sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7\" \
          crossorigin=\"anonymous\">
  </head>
  <body>
    <div class=\"container\" role=\"main\">
      <h1>FAILURE!</h1>
      <h2>We were unable to authorize the git-box application!</h2>
      <h2>You may safely close this page.</h2>
    </div>
    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"
            integrity=\"sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS\" \
            crossorigin=\"anonymous\"></script>
  </body>
</html>";
