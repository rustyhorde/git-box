use {Args, to_hex};
use chrono::{DateTime, Duration, UTC};
use chrono::offset::TimeZone;
use config::{self, Configure, GitBoxConfigResult};
use curl::http;
use error::GitBoxError;
use filter::{Clean, Filter, GitBoxFilterResult, Smudge, repo};
use filter::cloud::{FAIL_RESP_HTML, SUCC_RESP_HTML};
use git2::Config;
use hyper;
use hyper::server::{Handler, Request, Response, Server};
use hyper::status::StatusCode;
use hyper::uri::RequestUri;
use rustc_serialize::base64::{ToBase64, URL_SAFE};
use serde_json;
use sodium_sys::crypto::hash::generichash;
use sodium_sys::crypto::utils::{init, randombytes};
use std::collections::HashSet;
use std::sync::mpsc::{Sender, channel};
use std::sync::Mutex;
use std::thread;
use url::Url;
use url::percent_encoding::{QUERY_ENCODE_SET, percent_encode};
use url_open::UrlOpen;

const DATE_TIME_FMT: &'static str = "%Y%m%dT%H%M%SZ";
const GOOGLE_OAUTH2_URL: &'static str =
    "https://accounts.google.com/o/oauth2/v2/auth?response_type=code&scope=https://www.googleapis.\
     com/auth/drive.appfolder https://www.googleapis.com/auth/drive.appdata \
     https://www.googleapis.com/auth/drive.\
     file&client_id=1070895153172-sdsimhg09gskco5uhrrgrmvcg5r75t5s.apps.googleusercontent.\
     com&redirect_uri=http://localhost:3000/git-box&access_type=offline&state=";
const GOOGLE_TOKEN_URL: &'static str = "https://www.googleapis.com/oauth2/v4/token";
const GOOGLE_UPLOAD_URL: &'static str = "https://www.googleapis.com\
                                        /upload/drive/v3/files?uploadType=multipart";
const GOOGLE_FILES_URL: &'static str = "https://www.googleapis.com/drive/v3/files/";

/// Google Drive Binary Off-Repository eXternal Storage
pub struct GoogleDriveBox {
    /// Google Drive API Access Token
    access_token: String,
    /// Google Drive API Refresh Token (Access Tokens expire after an hour).
    refresh_token: String,
    /// The datetime the access token expires.
    expires: DateTime<UTC>,
    /// Was this filter configured globally?
    global: bool,
}

impl Default for GoogleDriveBox {
    fn default() -> GoogleDriveBox {
        GoogleDriveBox {
            refresh_token: String::new(),
            access_token: String::new(),
            expires: UTC::now(),
            global: false,
        }
    }
}

struct SenderHandler {
    sender: Mutex<Sender<Vec<(String, String)>>>,
}

impl Handler for SenderHandler {
    fn handle(&self, req: Request, mut res: Response) {
        let mut send = vec![];
        if let (hyper::Get, RequestUri::AbsolutePath(ref pqf)) = (req.method, req.uri) {
            if let Some(pqff) = pqf.path.first() {
                if let "git-box" = &pqff[..] {
                    if let Some(qp) = pqf.query_pairs() {
                        send = qp;
                        *res.status_mut() = StatusCode::Ok;
                        res.send(SUCC_RESP_HTML.as_bytes()).expect("Unable to send response!");
                    } else {
                        *res.status_mut() = StatusCode::BadRequest;
                        res.send(FAIL_RESP_HTML.as_bytes()).expect("Unable to send response!");
                    }
                } else {
                    *res.status_mut() = StatusCode::NotFound;
                    res.send(FAIL_RESP_HTML.as_bytes()).expect("Unable to send response!");
                }
            } else {
                *res.status_mut() = StatusCode::BadRequest;
                res.send(FAIL_RESP_HTML.as_bytes()).expect("Unable to send response!");
            }
        } else {
            *res.status_mut() = StatusCode::MethodNotAllowed;
            res.send(FAIL_RESP_HTML.as_bytes()).expect("Unable to send response!");
        }

        if let Ok(l) = self.sender.lock() {
            l.send(send).expect("Unable to send query parameters!");
        }
    }
}

impl From<Config> for GoogleDriveBox {
    fn from(config: Config) -> GoogleDriveBox {
        GoogleDriveBox {
            access_token: match config.get_string("box.google.accesstoken") {
                Ok(u) => u,
                Err(_) => "".to_owned(),
            },
            refresh_token: match config.get_string("box.google.refreshtoken") {
                Ok(u) => u,
                Err(_) => "".to_owned(),
            },
            expires: match config.get_string("box.google.expires") {
                Ok(e) => UTC.datetime_from_str(&e, DATE_TIME_FMT).unwrap_or_else(|_| UTC::now()),
                Err(_) => UTC::now(),
            },
            global: match config.get_bool("box.google.global") {
                Ok(g) => g,
                Err(_) => false,
            },
        }
    }
}

impl From<Args> for GoogleDriveBox {
    fn from(args: Args) -> GoogleDriveBox {
        let mut gdbox: GoogleDriveBox = Default::default();

        // Setup the random state string.
        init::init();
        let mut state_buf = [0; 30];
        randombytes::random_byte_array(&mut state_buf);
        let state = state_buf.to_base64(URL_SAFE);

        // Setup the Google OAuth2 url.
        let mut url = GOOGLE_OAUTH2_URL.to_owned();
        url.push_str(&state);

        // Open a browser to prompt for app permissions.
        let parsed = Url::parse(&url).expect("Unable to parse url!");
        UrlOpen::open(&parsed);

        // Spin up a hyper server to listen for the callback.
        let (tx, rx) = channel();

        thread::spawn(move || {
            let server = Server::http("127.0.0.1:3000").expect("Unable to create hyper server!");
            server.handle(SenderHandler { sender: Mutex::new(tx) })
                .expect("Unable to setup hello handler!");
        });

        // Wait for the query parameters to come back
        let qps = rx.recv().expect("Unable to receive!");
        let mut valid = false;
        let mut code = String::new();

        for &(ref k, ref v) in &qps {
            match &k[..] {
                "state" => valid = v == &state,
                "code" => code.push_str(v),
                _ => {}
            }
        }

        // If the state matches and the code isn't empty, send out an auth request.
        if valid && !code.is_empty() {
            let body = format!("code={}&grant_type=authorization_code&client_id=1070895153172-sds\
                                imhg09gskco5uhrrgrmvcg5r75t5s.apps.googleusercontent.\
                                com&client_secret=rO7GvJrhSYu7lPRKbZl5h8xk&redirect_uri=http:\
                                //localhost:3000/git-box",
                               code);
            let resp = http::handle()
                .post(GOOGLE_TOKEN_URL, &body[..])
                .header("Host", "www.googleapis.com")
                .header("Content-Type", "application/x-www-form-urlencoded")
                .exec()
                .expect("Unable to post token request!");

            let json = String::from_utf8_lossy(resp.get_body());
            if let Ok(refresh_json) = serde_json::from_str::<RefreshJson>(&json) {
                if let Some(token) = args.flag_token {
                    gdbox.refresh_token = token;
                } else {
                    let refresh_token = refresh_json.refresh_token;
                    if let Some(r) = refresh_token {
                        gdbox.refresh_token = r;
                    }
                }

                gdbox.access_token = refresh_json.access_token;

                let expires = refresh_json.expires_in;
                let from_now = Duration::seconds(expires);
                let then = UTC::now().checked_add(from_now).expect("Unable to set then!");
                gdbox.expires = then;
            }
        }

        gdbox.global = args.flag_global;
        gdbox
    }
}

impl Configure for GoogleDriveBox {
    fn set_config(&self, config: &mut Config) -> GitBoxConfigResult {
        let expiresfmt = self.expires.format(DATE_TIME_FMT).to_string();
        try!(config.set_str("box.storagetype", "GOOGLE"));
        try!(config.set_str("box.google.accesstoken", &self.access_token[..]));
        try!(config.set_str("box.google.refreshtoken", &self.refresh_token[..]));
        try!(config.set_str("box.google.expires", &expiresfmt[..]));
        try!(config.set_bool("box.google.global", self.global));

        if ::enabled() {
            debug!("Set git-box GOOGLE config:");
            debug!("  box.storagetype:         {}", "GOOGLE");
            debug!("  box.google.accesstoken:  {}", self.access_token);
            debug!("  box.google.refreshtoken: {}", self.refresh_token);
            debug!("  box.google.expires:      {}", expiresfmt);
            debug!("  box.google.global:       {}", self.global);
        }

        Ok(0)
    }

    fn remove_config(&self, config: &mut Config) -> GitBoxConfigResult {
        try!(config.remove("box.storagetype"));
        try!(config.remove("box.google.accesstoken"));
        try!(config.remove("box.google.refreshtoken"));
        try!(config.remove("box.google.expires"));
        try!(config.remove("box.google.global"));

        if ::enabled() {
            debug!("Unset git-box GOOGLE config");
        }

        Ok(0)
    }
}

#[derive(Deserialize, Debug)]
struct FileSizeJson {
    size: u64,
    id: String,
}

#[derive(Deserialize, Debug)]
struct FilesJson {
    files: Vec<FileSizeJson>,
}

#[derive(Deserialize, Debug)]
struct RefreshJson {
    access_token: String,
    refresh_token: Option<String>,
    expires_in: i64,
}

#[allow(dead_code)]
impl GoogleDriveBox {
    fn refresh(&self) -> Result<String, GitBoxError> {
        let now = UTC::now();
        if now < self.expires {
            Ok(self.access_token.clone())
        } else {
            let body = format!("grant_type=refresh_token&client_id=1070895153172-sds\
                                imhg09gskco5uhrrgrmvcg5r75t5s.apps.googleusercontent.\
                                com&client_secret=rO7GvJrhSYu7lPRKbZl5h8xk&refresh_token={}",
                               self.refresh_token);
            let resp = http::handle()
                .post(GOOGLE_TOKEN_URL, &body[..])
                .header("Host", "www.googleapis.com")
                .header("Content-Type", "application/x-www-form-urlencoded")
                .exec()
                .expect("Unable to post token request!");

            let json = String::from_utf8_lossy(resp.get_body());
            let refresh_json: RefreshJson = try!(serde_json::from_str(&json));
            let mut config = try!(config::open_config(self.global));

            let access_token = refresh_json.access_token;
            try!(config.set_str("box.google.accesstoken", &access_token));

            let expires = refresh_json.expires_in;
            let from_now = Duration::seconds(expires);
            let then = UTC::now().checked_add(from_now).expect("Unable to set then!");
            let thenfmt = then.format(DATE_TIME_FMT).to_string();
            try!(config.set_str("box.google.expires", &thenfmt));

            Ok(access_token)
        }
    }

    fn meta(&self, hash: &str, content_length: usize, access_token: &str) -> GitBoxFilterResult {
        // Setup the URL
        let mut url = GOOGLE_FILES_URL.to_owned();
        let mut query = format!("?q=name='{}'", hash);
        query.push_str("&fields=files(id,size)");
        let enc_query = percent_encode(&query.as_bytes(), QUERY_ENCODE_SET);
        url.push_str(&enc_query[..]);

        // Setup the request headers
        let bearer = format!("Bearer {}", access_token);
        let mut headers = HashSet::new();
        headers.insert(("Authorization", &bearer[..]));

        let resp = try!(repo::get(&url, headers, None));

        if let 200 = resp.get_code() {
            let json = String::from_utf8_lossy(resp.get_body());
            let files_json: FilesJson = try!(serde_json::from_str(&json));
            if let Some(file) = files_json.files.first() {
                let size = file.size;

                if size == content_length as u64 {
                    Ok(content_length)
                } else {
                    Err(GitBoxError::Other("Sizes do not match!"))
                }

            } else {
                Err(GitBoxError::Other("Found more than one matching file!"))
            }
        } else {
            try!(repo::stderr_resp(resp));
            Err(GitBoxError::Other("Unable to request file metadata!"))
        }
    }
}

impl Clean for GoogleDriveBox {
    fn clean(&self, buf: &mut Vec<u8>, hash: &str, size: usize) -> GitBoxFilterResult {
        // Check the buffer matches given hash and length.
        let chkhash = to_hex(try!(generichash::hash(&buf[..], None, None)));
        assert!(chkhash == hash);
        let content_length = buf.len();
        assert!(content_length == size);

        let access_token = try!(self.refresh());

        self.meta(&chkhash, content_length, &access_token).or_else(|_| {
            // Setup the request headers
            let bearer = format!("Bearer {}", access_token);
            let mut multi: Vec<u8> = Vec::new();
            let name_json = format!("{{ \"name\": \"{}\" }}\r\n\r\n", chkhash);
            multi.extend_from_slice(b"--git-box\r\n");
            multi.extend_from_slice(b"Content-Type: application/json;");
            multi.extend_from_slice(b"charset=UTF-8\r\n\r\n");
            multi.extend_from_slice(name_json.as_bytes());
            multi.extend_from_slice(b"--git-box\r\n");
            multi.extend_from_slice(b"Content-Type: application/octet-stream\r\n\r\n");
            multi.extend_from_slice(&buf[..]);
            multi.extend_from_slice(b"\r\n");
            multi.extend_from_slice(b"--git-box--");

            let cl = multi.len().to_string();

            let mut headers = HashSet::new();
            headers.insert(("Authorization", &bearer[..]));
            headers.insert(("Content-Length", &cl[..]));
            headers.insert(("Content-Type", "multipart/related boundary=git-box"));

            // Send the request
            let resp =
                try!(repo::post(GOOGLE_UPLOAD_URL, &multi[..], headers, Some(3600000), false));

            if let 200 = resp.get_code() {
                Ok(size)
            } else {
                try!(repo::stderr_resp(resp));
                Err(GitBoxError::Other("Error putting file in Google Drive folder!"))
            }
        })
    }
}

impl Smudge for GoogleDriveBox {
    fn smudge(&self, buf: &mut Vec<u8>, hash: &str, size: usize) -> GitBoxFilterResult {
        let access_token = try!(self.refresh());

        // Setup the URL
        let mut url = GOOGLE_FILES_URL.to_owned();
        let mut query = format!("?q=name='{}'", hash);
        query.push_str("&fields=files(id,size)");
        let enc_query = percent_encode(&query.as_bytes(), QUERY_ENCODE_SET);
        url.push_str(&enc_query[..]);

        // Setup the request headers
        let bearer = format!("Bearer {}", access_token);
        let mut headers = HashSet::new();
        headers.insert(("Authorization", &bearer[..]));

        let resp = try!(repo::get(&url, headers, None));

        if let 200 = resp.get_code() {
            let json = String::from_utf8_lossy(resp.get_body());
            let files_json: FilesJson = try!(serde_json::from_str(&json));
            if files_json.files.len() == 1 {
                if let Some(file) = files_json.files.first() {
                    // Setup the download URL
                    let mut dl_url = GOOGLE_FILES_URL.to_owned();
                    dl_url.push_str(&file.id);
                    dl_url.push_str("?alt=media");

                    // Setup the request headers
                    let dl_bearer = format!("Bearer {}", access_token);
                    let mut dl_headers = HashSet::new();
                    dl_headers.insert(("Authorization", &dl_bearer[..]));

                    let dl_resp = try!(repo::get(&dl_url, dl_headers, Some(360000)));

                    if let 200 = dl_resp.get_code() {
                        buf.extend(dl_resp.get_body());

                        // Check the buffer matches given hash and length.
                        let chkhash = to_hex(try!(generichash::hash(&buf[..], None, None)));
                        assert!(chkhash == hash);
                        let content_length = buf.len();
                        assert!(content_length == size);

                        Ok(size)
                    } else {
                        try!(repo::stderr_resp(dl_resp));
                        Err(GitBoxError::Other("Error downloading file!"))
                    }
                } else {
                    Err(GitBoxError::Other("Unable to determine file!"))
                }
            } else {
                Err(GitBoxError::Other("Found more than one matching file!"))
            }
        } else {
            try!(repo::stderr_resp(resp));
            Err(GitBoxError::Other("Unable to request file metadata!"))
        }
    }
}

impl Filter for GoogleDriveBox {}
