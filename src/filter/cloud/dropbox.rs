use {Args, to_hex};
use config::{Configure, GitBoxConfigResult};
use curl::http;
use error::GitBoxError;
use filter::{Clean, Filter, GitBoxFilterResult, Smudge, repo};
use filter::cloud::{FAIL_RESP_HTML, SUCC_RESP_HTML};
use git2::Config;
use hyper;
use hyper::server::{Handler, Request, Response, Server};
use hyper::status::StatusCode;
use hyper::uri::RequestUri;
use rustc_serialize::base64::{ToBase64, URL_SAFE};
use serde_json;
use sodium_sys::crypto::hash::generichash;
use sodium_sys::crypto::utils::{init, randombytes};
use std::collections::HashSet;
use std::sync::mpsc::{Sender, channel};
use std::sync::Mutex;
use std::thread;
use url::Url;
use url_open::UrlOpen;

const DROPBOX_OAUTH2_URL: &'static str = "https://www.dropbox.\
                                          com/1/oauth2/authorize?response_type=code&client_id=i9yo0kph4eibwtk&redirect_uri=http:\
                                          //localhost:3000/git-box-rs&state=";
const DROPBOX_UPLOAD_URL: &'static str = "https://content.dropboxapi.com/2/files/upload";
const DROPBOX_META_URL: &'static str = "https://api.dropboxapi.com/2/files/get_metadata";
const DROPBOX_DOWNLOAD_URL: &'static str = "https://content.dropboxapi.com/2/files/download";

#[derive(Default)]
/// Dropbox Binary Off-Repository eXternal Storage
pub struct DropboxBox {
    /// Dropbox API Bearer Token
    token: String,
}

struct SenderHandler {
    sender: Mutex<Sender<Vec<(String, String)>>>,
}

impl Handler for SenderHandler {
    fn handle(&self, req: Request, mut res: Response) {
        let mut send = vec![];
        if let (hyper::Get, RequestUri::AbsolutePath(ref pqf)) = (req.method, req.uri) {
            if let Some(pqff) = pqf.path.first() {
                if let "git-box-rs" = &pqff[..] {
                    if let Some(qp) = pqf.query_pairs() {
                        send = qp;
                        *res.status_mut() = StatusCode::Ok;
                        res.send(SUCC_RESP_HTML.as_bytes()).expect("Unable to send response!");
                    } else {
                        *res.status_mut() = StatusCode::BadRequest;
                        res.send(FAIL_RESP_HTML.as_bytes()).expect("Unable to send response!");
                    }
                } else {
                    *res.status_mut() = StatusCode::NotFound;
                    res.send(FAIL_RESP_HTML.as_bytes()).expect("Unable to send response!");
                }
            } else {
                *res.status_mut() = StatusCode::BadRequest;
                res.send(FAIL_RESP_HTML.as_bytes()).expect("Unable to send response!");
            }
        } else {
            *res.status_mut() = StatusCode::MethodNotAllowed;
            res.send(FAIL_RESP_HTML.as_bytes()).expect("Unable to send response!");
        }

        if let Ok(l) = self.sender.lock() {
            l.send(send).expect("Unable to send query parameters!");
        }
    }
}

impl From<Config> for DropboxBox {
    fn from(config: Config) -> DropboxBox {
        DropboxBox {
            token: match config.get_string("box.dropbox.token") {
                Ok(u) => u,
                Err(_) => "".to_owned(),
            },
        }
    }
}

#[derive(Deserialize, Debug)]
struct AuthJson {
    access_token: String,
}

impl From<Args> for DropboxBox {
    fn from(_args: Args) -> DropboxBox {
        let mut dropbox: DropboxBox = Default::default();

        // Setup the random state string.
        init::init();
        let mut state_buf = [0; 16];
        randombytes::random_byte_array(&mut state_buf);
        let state = state_buf.to_base64(URL_SAFE);

        // Setup the Dropbox OAuth2 url.
        let mut url = DROPBOX_OAUTH2_URL.to_owned();
        url.push_str(&state);

        // Open a browser to prompt for app permissions.
        let parsed = Url::parse(&url).expect("Unable to parse url!");
        UrlOpen::open(&parsed);

        // Spin up a hyper server to listen for the callback.
        let (tx, rx) = channel();

        thread::spawn(move || {
            let server = Server::http("127.0.0.1:3000").expect("Unable to create hyper server!");
            server.handle(SenderHandler { sender: Mutex::new(tx) })
                .expect("Unable to setup hello handler!");
        });

        // Wait for the query parameters to come back
        let qps = rx.recv().expect("Unable to receive!");
        let mut valid = false;
        let mut code = String::new();

        for &(ref k, ref v) in &qps {
            match &k[..] {
                "state" => valid = v == &state,
                "code" => code.push_str(v),
                _ => {}
            }
        }

        // If the state matches and the code isn't empty, send out an auth request.
        if valid && !code.is_empty() {
            let body =
                format!("code={}&grant_type=authorization_code&client_id=i9yo0kph4eibwtk&client_secret=g62cih0jnhutcwl&redirect_uri=http:\
                         //localhost:3000/git-box-rs",
                        code);
            let resp = http::handle()
                .post("https://api.dropbox.com/1/oauth2/token", &body[..])
                .header("Content-Type", "application/x-www-form-urlencoded")
                .exec()
                .expect("Unable to post token request!");
            let json = String::from_utf8_lossy(resp.get_body());
            if let Ok(auth_json) = serde_json::from_str::<AuthJson>(&json) {
                dropbox.token = auth_json.access_token;
            }
        }

        dropbox
    }
}

impl Configure for DropboxBox {
    fn set_config(&self, config: &mut Config) -> GitBoxConfigResult {
        try!(config.set_str("box.storagetype", "DROPBOX"));
        try!(config.set_str("box.dropbox.token", &self.token[..]));

        if ::enabled() {
            debug!("Set git-box DROPBOX config:");
            debug!("  box.storagetype:   {}", "DROPBOX");
            debug!("  box.dropbox.token: {}", self.token);
        }

        Ok(0)
    }

    fn remove_config(&self, config: &mut Config) -> GitBoxConfigResult {
        try!(config.remove("box.storagetype"));
        try!(config.remove("box.dropbox.token"));

        if ::enabled() {
            debug!("Unset git-box DROPBOX config");
        }

        Ok(0)
    }
}

#[derive(Deserialize, Debug)]
struct MetaJson {
    size: u64,
}

impl DropboxBox {
    fn meta(&self, hash: &str, content_length: usize) -> GitBoxFilterResult {
        // Setup the request headers
        let bearer = format!("Bearer {}", self.token);
        let mut headers = HashSet::new();
        let path = format!("{{\"path\": \"/{}\"}}", hash);

        headers.insert(("Authorization", &bearer[..]));
        headers.insert(("Content-Type", "application/json"));

        let resp = try!(repo::post(DROPBOX_META_URL, path.as_bytes(), headers, None, false));

        if let 200 = resp.get_code() {
            let json = String::from_utf8_lossy(resp.get_body());
            if let Ok(meta_json) = serde_json::from_str::<MetaJson>(&json) {
                if meta_json.size == content_length as u64 {
                    Ok(content_length)
                } else {
                    Err(GitBoxError::Other("Size does not match!"))
                }
            } else {
                Err(GitBoxError::Other("Error getting metadata!"))
            }
        } else {
            Err(GitBoxError::Other("Error getting metadata!"))
        }
    }
}

impl Clean for DropboxBox {
    fn clean(&self, buf: &mut Vec<u8>, hash: &str, size: usize) -> GitBoxFilterResult {
        // Check the buffer matches given hash and length.
        let chkhash = to_hex(try!(generichash::hash(&buf[..], None, None)));
        assert!(chkhash == hash);
        let content_length = buf.len();
        assert!(content_length == size);

        self.meta(&chkhash, content_length).or_else(|_| {
            // Setup the request headers
            let bearer = format!("Bearer {}", self.token);
            let path = format!("{{\"path\": \"/{}\"}}", &chkhash);
            let cl = content_length.to_string();
            let mut headers = HashSet::new();

            headers.insert(("Authorization", &bearer[..]));
            headers.insert(("Content-Length", &cl[..]));
            headers.insert(("Content-Type", "application/octet-stream"));
            headers.insert(("Dropbox-API-Arg", &path[..]));

            // Send the request
            let resp = try!(repo::post(DROPBOX_UPLOAD_URL, buf, headers, Some(3600000), false));

            if let 200 = resp.get_code() {} else {
                try!(repo::stderr_resp(resp));
                return Err(GitBoxError::Other("Error putting file in Dropbox folder!"));
            }

            Ok(size)
        })
    }
}

impl Smudge for DropboxBox {
    fn smudge(&self, buf: &mut Vec<u8>, hash: &str, size: usize) -> GitBoxFilterResult {
        // Setup the request headers
        let bearer = format!("Bearer {}", self.token);
        let path = format!("{{\"path\": \"/{}\"}}", hash);
        let mut headers = HashSet::new();
        headers.insert(("Authorization", &bearer[..]));
        headers.insert(("Dropbox-API-Arg", &path[..]));

        // Send the request
        let resp = try!(repo::get(DROPBOX_DOWNLOAD_URL, headers, Some(3600000)));

        if let 200 = resp.get_code() {} else {
            try!(repo::stderr_resp(resp));
            return Err(GitBoxError::Other("Error putting file in Dropbox folder!"));
        }

        // Check the buffer matches given hash and length.
        buf.extend(resp.get_body());
        assert!(size == buf.len());
        let chkhash = to_hex(try!(generichash::hash(&buf[..], None, None)));
        assert!(chkhash == hash);

        Ok(size)
    }
}

impl Filter for DropboxBox {}
