use {Args, to_hex};
use config::{Configure, GitBoxConfigResult};
use error::GitBoxError;
use filter::{Clean, Filter, GitBoxFilterResult, Smudge};
use git2::Config;
use mysql::conn::{MyConn, MyOpts};
use mysql::value::{ToValue, from_row};
use sodium_sys::crypto::hash::generichash;
use std::fmt;

#[derive(Clone, Default)]
/// Mysql Binary Off-Repository eXternal Storage
pub struct MysqlBox {
    // Username to connect with.
    user: Option<String>,
    // Password to connect with.
    password: Option<String>,
    // Host to connect to.
    host: Option<String>,
    // Port to connect to.
    port: Option<u16>,
    // On unix, the socket address can be specified rather than host:port.
    socketaddr: Option<String>,
    // Database name
    database: Option<String>,
    // Table name
    table: Option<String>,
    // Prefer a socket connection?
    prefer_socket: bool,
    // Is SSL enabled?
    ssl: bool,
    // Verify peer?
    verify_peer: bool,
}

impl MysqlBox {
    fn table(&self) -> String {
        match self.table {
            Some(ref t) => t.clone(),
            None => "git_box".to_owned(),
        }
    }

    fn remove(&self, config: &mut Config, name: &str) -> Result<(), GitBoxError> {
        Ok(if let Ok(_) = config.get_string(name) {
            try!(config.remove(name));
        })
    }
}

impl Into<MyOpts> for MysqlBox {
    fn into(self) -> MyOpts {
        use std::path::PathBuf;

        let mut opts = MyOpts {
            user: self.user,
            pass: self.password,
            db_name: self.database,
            prefer_socket: self.prefer_socket,
            verify_peer: self.verify_peer,
            ..Default::default()
        };

        if let Some(h) = self.host {
            opts.tcp_addr = Some(h);
        }

        if let Some(p) = self.port {
            opts.tcp_port = p;
        }

        if let Some(sa) = self.socketaddr {
            opts.unix_addr = Some(PathBuf::from(sa));
        }

        opts
    }
}

impl From<Args> for MysqlBox {
    fn from(args: Args) -> MysqlBox {
        MysqlBox {
            user: args.flag_u,
            password: args.flag_p,
            host: args.flag_h,
            port: args.flag_port,
            socketaddr: args.flag_socket,
            database: args.flag_d,
            table: args.flag_t,
            prefer_socket: args.flag_prefer,
            ssl: args.flag_ssl,
            verify_peer: args.flag_verify,
        }
    }
}

#[cfg_attr(feature = "clippy", allow(cast_possible_truncation, cast_sign_loss))]
impl From<Config> for MysqlBox {
    fn from(config: Config) -> MysqlBox {
        MysqlBox {
            user: match config.get_string("box.mysql.user") {
                Ok(u) => Some(u.to_owned()),
                Err(_) => None,
            },
            password: match config.get_string("box.mysql.pass") {
                Ok(p) => Some(p.to_owned()),
                Err(_) => None,
            },
            host: match config.get_string("box.mysql.host") {
                Ok(h) => Some(h.to_owned()),
                Err(_) => None,
            },
            port: match config.get_i64("box.mysql.port") {
                Ok(p) => Some(p as u16),
                Err(_) => None,
            },
            socketaddr: match config.get_string("box.mysql.socketaddr") {
                Ok(sa) => Some(sa.to_owned()),
                Err(_) => None,
            },
            database: match config.get_string("box.mysql.db") {
                Ok(d) => Some(d.to_owned()),
                Err(_) => None,
            },
            table: match config.get_string("box.mysql.table") {
                Ok(t) => Some(t.to_owned()),
                Err(_) => None,
            },
            prefer_socket: match config.get_bool("box.mysql.prefer") {
                Ok(b) => b,
                Err(_) => true,
            },
            ssl: match config.get_bool("box.mysql.ssl") {
                Ok(b) => b,
                Err(_) => false,
            },
            verify_peer: match config.get_bool("box.mysql.verify") {
                Ok(b) => b,
                Err(_) => false,
            },
        }
    }
}

fn safe_set_str(config: &mut Config, key: &str, val: &Option<String>) -> Result<(), GitBoxError> {
    if let Some(ref p) = *val {
        try!(config.set_str(key, &p[..]));
    }
    Ok(())
}

fn opt_debug<T>(key: &str, val: &Option<T>)
    where T: fmt::Display
{
    if let Some(ref v) = *val {
        debug!("  {}: {}", key, v);
    }
}

impl Configure for MysqlBox {
    fn set_config(&self, config: &mut Config) -> GitBoxConfigResult {
        try!(config.set_str("box.storagetype", "MYSQL"));
        try!(safe_set_str(config, "box.mysql.user", &self.user));
        try!(safe_set_str(config, "box.mysql.pass", &self.password));
        try!(safe_set_str(config, "box.mysql.host", &self.host));

        if let Some(ref p) = self.port {
            try!(config.set_i64("box.mysql.port", *p as i64));
        }

        try!(safe_set_str(config, "box.mysql.socket", &self.socketaddr));
        try!(safe_set_str(config, "box.mysql.db", &self.database));
        try!(safe_set_str(config, "box.mysql.table", &self.table));
        try!(config.set_bool("box.mysql.prefer", self.prefer_socket));
        try!(config.set_bool("box.mysql.ssl", self.ssl));
        try!(config.set_bool("box.mysql.verify", self.verify_peer));

        if ::enabled() {
            debug!("Set git-box MYSQL config:");
            debug!("  box.storagetype:  {}", "MYSQL");
            opt_debug("  box.mysql.user", &self.user);
            opt_debug("  box.mysql.pass", &self.password);
            opt_debug("  box.mysql.host", &self.host);
            opt_debug("  box.mysql.port", &self.port);
            opt_debug("  box.mysql.socket", &self.socketaddr);
            opt_debug("  box.mysql.db", &self.database);
            opt_debug("  box.mysql.table", &self.table);
            debug!("  box.mysql.prefer: {}", self.prefer_socket);
            debug!("  box.mysql.ssl:    {}", self.ssl);
            debug!("  box.mysql.verify: {}", self.verify_peer);
        }

        Ok(0)
    }

    fn remove_config(&self, config: &mut Config) -> GitBoxConfigResult {
        try!(config.remove("box.storagetype"));
        try!(self.remove(config, "box.mysql.user"));
        try!(self.remove(config, "box.mysql.pass"));
        try!(self.remove(config, "box.mysql.host"));
        try!(self.remove(config, "box.mysql.port"));
        try!(self.remove(config, "box.mysql.socket"));
        try!(self.remove(config, "box.mysql.db"));
        try!(self.remove(config, "box.mysql.table"));
        try!(config.remove("box.mysql.prefer"));
        try!(config.remove("box.mysql.ssl"));
        try!(config.remove("box.mysql.verify"));

        if ::enabled() {
            debug!("Unset git-box MYSQL config");
        }

        Ok(0)
    }
}

impl Clean for MysqlBox {
    fn clean(&self, buf: &mut Vec<u8>, hash: &str, size: usize) -> GitBoxFilterResult {
        // Check the buffer matches given hash and length.
        let chkhash = to_hex(try!(generichash::hash(&buf[..], None, None)));
        assert!(chkhash == hash);
        assert!(buf.len() == size);

        let create_sql = format!("CREATE TABLE IF NOT EXISTS {} (id CHAR(65), box LONGBLOB NOT \
                                  NULL, PRIMARY KEY (id)) Engine=InnoDB",
                                 &self.table());
        let insert_sql = format!("INSERT INTO {} (id, box) VALUES (?,?) ON DUPLICATE KEY UPDATE \
                                  box=?",
                                 &self.table());

        let opts: MyOpts = (*self).clone().into();
        let mut conn = try!(MyConn::new(opts));
        try!(conn.prep_exec(&create_sql[..], ()));
        let params: &[&ToValue] = &[&chkhash, buf, buf];
        let mut stmt = try!(conn.prepare(&insert_sql[..]));
        try!(stmt.execute(params));

        Ok(size)
    }
}

impl Smudge for MysqlBox {
    fn smudge(&self, buf: &mut Vec<u8>, hash: &str, size: usize) -> GitBoxFilterResult {
        let select_sql = format!("SELECT box FROM {} WHERE id = ?", &self.table());
        let opts: MyOpts = (*self).clone().into();
        let mut conn = try!(MyConn::new(opts));
        let params: &[&ToValue] = &[&hash];
        let mut stmt = try!(conn.prepare(&select_sql[..]));
        let result = try!(stmt.execute(params));

        for row in result {
            if let Ok(r) = row {
                let b = from_row::<Vec<u8>>(r);
                buf.extend_from_slice(&b);
            }
        }

        // Check the buffer matches given hash and length.
        let chkhash = to_hex(try!(generichash::hash(&buf[..], None, None)));
        assert!(chkhash == hash);
        assert!(buf.len() == size);

        Ok(size)
    }
}

impl Filter for MysqlBox {}
