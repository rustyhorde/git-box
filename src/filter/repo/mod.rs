use curl::http::{self, Headers, Response};
use error::GitBoxError;
use rustc_serialize::base64::{STANDARD, ToBase64};
use std::collections::HashSet;
use std::io::{self, Write};

#[cfg(feature = "archiva")]
pub mod archiva;
#[cfg(feature = "artifactory")]
pub mod art;
#[cfg(feature = "nexus")]
pub mod nexus;

fn basic_auth(user: &str, pass: &str) -> String {
    let mut auth = user.to_owned();
    auth.push(':');
    auth.push_str(&pass[..]);

    let enc = auth.as_bytes().to_base64(STANDARD);
    let mut basic = String::from("Basic ");
    basic.push_str(&enc[..]);
    basic
}

fn base_url(host: &str, repo: &str, repodir: &str) -> Result<String, GitBoxError> {
    // Setup the Base URL: <url> / <repo> / <repodir>
    let mut url = String::from(host[..].trim_matches('/'));
    url.push('/');
    url.push_str(&repo[..]);
    url.push('/');
    url.push_str(&repodir[..]);
    Ok(url)
}

#[derive(Default)]
pub struct HttpResponse {
    code: u32,
    body: Vec<u8>,
    headers: Headers,
}

impl HttpResponse {
    pub fn get_code(&self) -> u32 {
        self.code
    }

    pub fn get_body(&self) -> &[u8] {
        &self.body
    }

    pub fn get_header<'a>(&'a self, name: &str) -> &'a [String] {
        self.headers
            .get(name)
            .map_or(&[], |v| &v[..])
    }
}

impl From<Response> for HttpResponse {
    fn from(resp: Response) -> HttpResponse {
        HttpResponse {
            code: resp.get_code(),
            headers: resp.get_headers().clone(),
            body: resp.move_body(),
        }
    }
}

pub type CurlResponse<T> = Result<T, GitBoxError>;

pub fn get(url: &str,
           headers: HashSet<(&str, &str)>,
           timeout: Option<usize>)
           -> CurlResponse<HttpResponse> {
    let mut handle = http::handle();

    if let Some(t) = timeout {
        handle = handle.connect_timeout(t).timeout(t);
    }

    let req = handle.get(&url[..]);
    let resp = try!(req.headers(headers.into_iter()).exec());

    Ok(HttpResponse::from(resp))
}

pub fn head(url: &str,
            headers: HashSet<(&str, &str)>,
            timeout: Option<usize>)
            -> CurlResponse<HttpResponse> {
    let mut handle = http::handle();

    if let Some(t) = timeout {
        handle = handle.connect_timeout(t).timeout(t);
    }

    let req = handle.head(&url[..]);
    let resp = try!(req.headers(headers.into_iter()).exec());

    Ok(HttpResponse::from(resp))
}

#[cfg(any(feature = "dropbox", feature="google"))]
pub fn post(url: &str,
            buf: &[u8],
            headers: HashSet<(&str, &str)>,
            timeout: Option<usize>,
            expect_continue: bool)
            -> CurlResponse<HttpResponse> {
    let mut handle = http::handle();

    if let Some(t) = timeout {
        handle = handle.connect_timeout(t).timeout(t);
    }

    let mut req = handle.post(&url[..], &buf[..]);

    if expect_continue {
        req = req.expect_continue();
    }

    let resp = try!(req.headers(headers.into_iter()).exec());

    Ok(HttpResponse::from(resp))
}

pub fn put(url: &str,
           buf: &[u8],
           headers: HashSet<(&str, &str)>,
           timeout: Option<usize>,
           expect_continue: bool)
           -> CurlResponse<HttpResponse> {
    let mut handle = http::handle();

    if let Some(t) = timeout {
        handle = handle.connect_timeout(t).timeout(t);
    }

    let mut req = handle.put(&url[..], &buf[..]);

    if expect_continue {
        req = req.expect_continue();
    }

    let resp = try!(req.headers(headers.into_iter()).exec());

    Ok(HttpResponse::from(resp))
}

pub fn stderr_resp(resp: HttpResponse) -> Result<(), GitBoxError> {
    let body_vec = resp.get_body();
    let body = String::from_utf8_lossy(&body_vec[..]);
    try!(writeln!(io::stderr(), "HTTP RESPONSE CODE: {}", resp.get_code()));
    try!(writeln!(io::stderr(), "{}", body));
    Ok(())
}
