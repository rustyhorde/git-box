#[cfg(feature = "s3")]
extern crate env_logger;
extern crate libgitbox;
#[macro_use]
extern crate log;
extern crate sodium_sys;

use libgitbox::{Filter, GitBoxFilterResult, box_storage};
use sodium_sys::crypto::utils::init;
use std::error::Error;
use std::io::{self, BufWriter, Read, Write};
use std::process;
use std::str::FromStr;
use std::sync::{ONCE_INIT, Once};

static START: Once = ONCE_INIT;

fn init_sodium() {
    START.call_once(|| {
        init::init();
    });
}

fn smudge<T: ?Sized>(filter: Box<T>) -> GitBoxFilterResult
    where T: Filter
{
    trace!("git-box-smudge");
    let mut buf = Vec::new();

    try!(io::stdin().read_to_end(&mut buf));

    let contents = try!(String::from_utf8(buf.clone()));
    let strs: Vec<&str> = contents.trim().split('$').collect();
    let hash = String::from(strs[0]);
    let size: usize = try!(FromStr::from_str(strs[1]));
    let mut outbuf = Vec::with_capacity(size);
    let mut bufw = BufWriter::new(io::stdout());

    let out = match filter.smudge(&mut outbuf, &hash[..], size) {
        Ok(_) => outbuf,
        Err(e) => {
            try!(writeln!(io::stderr(), "{:?}", e));
            buf
        }
    };

    try!(bufw.write_all(&out[..]));
    try!(bufw.flush());
    Ok(size)
}

fn stderr<T: Error>(e: T) {
    writeln!(io::stderr(), "{:?}", e).expect("Unable to write to stderr");
}

#[cfg(feature = "s3")]
fn init_env_logger() {
    env_logger::init().expect("Failed to initialize logging!");
}

#[cfg(not(feature = "s3"))]
fn init_env_logger() {}

pub fn main() {
    init_env_logger();
    init_sodium();
    match box_storage() {
        Ok(filter) => {
            match smudge(filter) {
                Ok(_) => process::exit(0),
                Err(e) => {
                    stderr(e);
                    process::exit(1);
                }
            }
        }
        Err(e) => {
            stderr(e);
            process::exit(1)
        }
    }
}
