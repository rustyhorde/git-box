// build.rs
extern crate vergen;

use vergen::*;

fn main() {
    let mut fns = OutputFns::all();
    fns.toggle(NOW);
    assert!(vergen(fns).is_ok());
}
