#[cfg(feature = "s3")]
extern crate env_logger;
extern crate libgitbox;
#[macro_use]
extern crate log;
extern crate sodium_sys;

use libgitbox::{Filter, GitBoxFilterResult, box_storage, to_hex};
use sodium_sys::crypto::utils::init;
use sodium_sys::crypto::hash::generichash;
use std::error::Error;
use std::io::{self, BufWriter, Read, Write};
use std::process;
use std::sync::{ONCE_INIT, Once};

static START: Once = ONCE_INIT;

fn init_sodium() {
    START.call_once(|| {
        init::init();
    });
}

fn clean<T: ?Sized>(filter: Box<T>) -> GitBoxFilterResult
    where T: Filter
{
    trace!("git-box-clean");
    let mut buf = Vec::new();

    try!(io::stdin().read_to_end(&mut buf));

    let size = buf.len();
    let hash = to_hex(try!(generichash::hash(&buf[..], None, None)));
    let hashed = format!("{}${}\n", &hash, size);

    let mut bufw = BufWriter::new(io::stdout());

    let outbuf = match filter.clean(&mut buf, &hash[..], size) {
        Ok(_) => hashed.as_bytes(),
        Err(e) => {
            try!(writeln!(io::stderr(), "{:?}", e));
            &buf[..]
        }
    };

    try!(bufw.write_all(outbuf));
    try!(bufw.flush());
    Ok(size)
}

fn stderr<T: Error>(e: T) {
    writeln!(io::stderr(), "{:?}", e).expect("Unable to write to stderr");
}

#[cfg(feature = "s3")]
fn init_env_logger() {
    env_logger::init().expect("Failed to initialize logging!");
}

#[cfg(not(feature = "s3"))]
fn init_env_logger() {}

pub fn main() {
    init_env_logger();
    init_sodium();
    match box_storage() {
        Ok(filter) => {
            match clean(filter) {
                Ok(_) => process::exit(0),
                Err(e) => {
                    stderr(e);
                    process::exit(1);
                }
            }
        }
        Err(e) => {
            stderr(e);
            process::exit(1)
        }
    }
}
